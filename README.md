# Simple Minio Config

I use minio to develop with S3 locally.

Deploy this on your dev box (assuming you're running Swarm already):
```bash
docker network create -d overlay --attachable astrometry && \
docker stack deploy -c docker-compose.yml minio
```

For any container on the `astrometry` network, minio will be accessible at `http://minio:9000` using the specified credentials.  It should also be accessible at http://localhost:9000 in your browser.
